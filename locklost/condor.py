import os
import sys
import pwd
import shutil
import collections
import subprocess
# import uuid
import logging

import datetime

from . import config

##################################################

def _write_executable(
        module,
        path,
        data_access='gwpy',
):
    """write locklost module condor job executable script

    The first argument is the name of the locklost module to be executed.

    """
    with open(path, 'w') as f:
        f.write("""#!/bin/sh
export IFO={}
export LOCKLOST_EVENT_ROOT={}
export PYTHONPATH={}
export DATA_ACCESS={}
export NDSSERVER={}
export LIGO_DATAFIND_SERVER={}
export CONDOR_ACCOUNTING_GROUP={}
export CONDOR_ACCOUNTING_GROUP_USER={}
exec {} -m locklost.{} "$@" 2>&1
""".format(
    config.IFO,
    config.EVENT_ROOT,
    os.getenv('PYTHONPATH', ''),
    data_access,
    os.getenv('NDSSERVER', ''),
    os.getenv('LIGO_DATAFIND_SERVER'),
    config.CONDOR_ACCOUNTING_GROUP,
    config.CONDOR_ACCOUNTING_GROUP_USER,
    sys.executable,
    module,
    ))
    os.chmod(path, 0o755)


class CondorSubmit(object):

    def __init__(self, condor_dir, module, args=[], log='', local=False, restart=False, notify_user=None):
        """Generate HTCondor submit file

        `args` is list of string argument names.

        """
        assert config.CONDOR_ACCOUNTING_GROUP
        assert config.CONDOR_ACCOUNTING_GROUP_USER
        self.condor_dir = os.path.abspath(condor_dir)
        self.module = module
        self.exec_path = os.path.join(condor_dir, module)
        self.sub_path = os.path.join(condor_dir, 'submit')
        if local:
            universe = 'local'
        else:
            universe = 'vanilla'
        if restart:
            on_exit_remove = '(ExitBySignal == False) && (ExitCode == 0)'
        else:
            on_exit_remove = 'True'
        submit = [
            ('executable', '{}'.format(self.exec_path)),
            ('arguments', ' '.join(args)),
            ('universe', universe),
            ('accounting_group', config.CONDOR_ACCOUNTING_GROUP),
            ('accounting_group_user', config.CONDOR_ACCOUNTING_GROUP_USER),
            ('getenv', 'True'),
            ('output', '{}/{}out'.format(self.condor_dir, log)),
            ('log', '{}/{}log'.format(self.condor_dir, log)),
            ('on_exit_remove', on_exit_remove),
            ('periodic_release', 'True'),
            ('kill_sig', config.CONDOR_KILL_SIGNAL),
        ]
        if notify_user:
            submit.append(('notification', 'Always'))
            submit.append(('notify_user', notify_user))
        self.submit_dict = collections.OrderedDict(submit)

    def write(self):
        _write_executable(self.module, self.exec_path)
        with open(self.sub_path, 'w') as f:
            for key, val in self.submit_dict.items():
                f.write('{} = {}\n'.format(key, val))
            f.write('queue 1\n')

    def submit(self):
        assert os.path.exists(self.sub_path), "Must write() before submitting"
        logging.info("condor submit: {}".format(self.sub_path))
        subprocess.call(['condor_submit', self.sub_path])

##################################################

class CondorDAG(object):

    def __init__(self, condor_dir, module, submit_args_gen, log=None, use_test_job=False):
        """Create HTCondor DAG

        `module` is the name of the module to be executed.
        `submit_args_gen` is a generator of command line arguments as
        a list of (key, value) tuples.

        The '<condor_dir>' directory will be created with the
        following components:

          <condor_dir>/dag     condor DAG file
          <condor_dir>/submit  condor submit file
          <condor_dir>/exec    locklost execution script
          <condor_dir>/logs/   log file directory

        """
        self.condor_dir = os.path.abspath(condor_dir)
        self.submit_args_gen = submit_args_gen
        self.use_test_job = use_test_job

        args = []
        # extract the argument names from the submit arg generator
        for sargs in self.submit_args_gen():
            args += ['$({})'.format(arg[0]) for arg in sargs]
            break

        if not log:
            log='logs/$(jobid)_$(cluster)_$(process).'

        self.sub = CondorSubmit(
            self.condor_dir,
            module,
            args,
            log=log,
        )

        self.dag_path = os.path.join(self.condor_dir, 'dag')

    # def __gen_job_id__(self):
    #     return str(uuid.uuid4())

    def __gen_VARS(self, *args):
        return ['{}="{}"'.format(k, v) for k, v in args]

    def as_string(self):
        s = ''
        for jid, sargs in enumerate(self.submit_args_gen()):
            VARS = self.__gen_VARS(('jobid', jid), *sargs)
            s += '''
JOB   {jid} {sub}
VARS  {jid} {VARS}
RETRY {jid} 1
'''.format(jid=jid,
           sub=self.sub.sub_path,
           VARS=' '.join(VARS),
       )
            if self.use_test_job and jid == 0:
                s += '''RETRY {jid} 1
'''.format(jid=jid)
        logging.info("condor DAG {} jobs".format(jid+1))
        return s

    @property
    def has_lock(self):
        lock = os.path.join(self.condor_dir, 'dag.lock')
        return os.path.exists(lock)

    def write(self):
        if self.has_lock:
            raise RuntimeError("DAG already running: {}".format(lock))
        shutil.rmtree(self.condor_dir)
        try:
            os.makedirs(os.path.join(self.condor_dir, 'logs'))
        except OSError:
            pass
        self.sub.write()
        with open(self.dag_path, 'w') as f:
            f.write(self.as_string())

    def submit(self):
        assert os.path.exists(self.dag_path), "Must write() before submitting"
        if self.has_lock:
            raise RuntimeError("DAG already running: {}".format(lock))
        logging.info("condor submit dag: {}".format(self.dag_path))
        subprocess.call(['condor_submit_dag', self.dag_path])
        print("""
Run the following to monitor condor job:
watch condor_q -nobatch
tail -F {0}.*
""".format(self.dag_path).strip())

##################################################

def find_jobs():
    """find all running condor jobs

    """
    import htcondor
    # user = os.getusername()
    user = pwd.getpwuid(os.getuid())[0]
    ojobs = []
    ajobs = []
    schedd = htcondor.Schedd()
    for job in schedd.xquery('Owner=?="{}"'.format(user)):
        job_type = job.get('Cmd').split('/')[-1]
        if job_type == 'online':
            ojobs.append(job)
        else:
            ajobs.append(job)
    return ojobs, ajobs


def job_str(job):
    """create status string for condor job

    """
    start = datetime.datetime.fromtimestamp(int(job.get('JobStartDate')))
    return '{}.{} {} {} {} {}'.format(
        job.get('ClusterId'),
        job.get('ProcId'),
        job.get('JobStatus'),
        start.isoformat('_'),
        job.get('Cmd'),
        job.get('Args') or '',
    )


def stop_jobs(job_list):
    """stop running condor jobs

    """
    import htcondor
    schedd = htcondor.Schedd()
    for job in job_list:
        logging.info("stopping job: {}".format(job_str(job)))
        schedd.act(
            htcondor.JobAction.Remove,
            'ClusterId=={} && ProcId=={}'.format(job['ClusterId'], job['ProcId']),
        )
