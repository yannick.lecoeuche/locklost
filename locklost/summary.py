import os
import argparse
import numpy as np

import matplotlib.pyplot as plt
from collections import defaultdict
from gpstime import gpsnow, gpstime
import pytz
import logging

from . import config
from .event import find_events
from . import plotutils


EPOCHS = {
    'run': config.O3_GPS_START,
    'month': int(gpsnow()) - 30*24*3600,
    'week': int(gpsnow()) - 7*24*3600,
}

if config.IFO == 'H1':
    local_tz = pytz.timezone('US/Pacific')
if config.IFO == 'L1':
    local_tz = pytz.timezone('US/Eastern')

def grab_data(gps):
    """Returns relevant lockloss summary data within three time ranges.

    Looks through O3 lockloss data and returns counts for the specificed check
    functions. Does this for the run, the last 30 days, and the last week.
    """
    shift_times = {
        'H1': [np.arange(0, 8), np.arange(8, 16), np.arange(16, 24)],
        'L1': [np.concatenate(([23], np.arange(0, 8))), np.arange(8, 12), np.arange(12, 22)]
    }
    shift_names = ['owl', 'day', 'eve']
    shifts = {
        shift_names[x]: {
            'time': shift_times[config.IFO][x],
            'counts': 0
        } for x in range(3)
    }
    transitions = defaultdict(int)
    observe_durations = []
    saturations = {
        'H1':
            {
                'LOCKING_ALS': defaultdict(int),
                'ACQUIRE_DRM1_1F': defaultdict(int),
                'Observe': defaultdict(int),
        },
        'L1':
            {
                'Observe': defaultdict(int)
        },
    }
    five_sats = []
    tag_count = defaultdict(int)
    tags = [
        'MAINTENANCE',
        'ADS_EXCURSION',
        'BOARD_SAT',
        'BRS_GLITCH',
        'EARTHQUAKE',
        'MICROSEISMIC',
        'ANTHROPOGENIC',
        'WINDY',
        'Unknown',
    ]

    event_count = 0
    for event in find_events(after=gps, state='0-{}'.format(config.GRD_NOMINAL_STATE[0])):
        transitions[event.transition_index[0]] += 1
        # check event/append to relevant lists
        observe_durations = check_durations(event, observe_durations)
        saturations, five_sats = check_saturations(event, saturations, five_sats)
        shifts = check_shift(event, shifts)
        tag_count = check_tags(event, tags, tag_count)
        event_count += 1

    logging.info("Events analyzed: {}".format(event_count))

    return transitions, observe_durations, saturations, five_sats, shifts, tag_count


def check_tags(event, tags, tag_count):
    """Checks event for most relevant tag to the lockloss.

    Checks event for tags, then increments the  and returns the 'tag_count'
    dictionary just once based on tag priority. Priority is given by the 'tags'
    list.
    """
    for tag in tags:
        if event.has_tag(tag):
            tag_count[tag] += 1
            break
    else:
        tag_count['Unknown'] += 1

    return tag_count


def check_shift(event, shifts):
    """Checks which operating shift event happened during.

    Checks which operator shift the lockloss gps happened during and increments
    a counter for that shift (for locklosses from Observe).
    """
    if not event.has_tag('OBSERVE'):
        return shifts
    gt = gpstime.fromgps(event.gps)
    gt = gt.astimezone(local_tz)
    for key in shifts:
        if gt.hour in shifts[key]['time']:
            shifts[key]['counts'] += 1
            break
    return shifts


def check_durations(event, durations):
    """Checks if lockloss was from Observe and logs lock duration. """
    if event.has_tag('OBSERVE'):
        previous = event.previous_state
        if previous:
            durations.append((previous['end']-previous['start'])/3600)
    return durations


def check_saturations(event, saturations, five_sats):
    """Checks for first five saturating suspensions around time of lockloss.

    Checks if lockloss is from one of the top three lockloss states, then
    logs which five suspensions saturated first into five_sats.
    """
    sat_path = event.path('saturations.csv')
    sat_conditions = {
        'H1': {
            'Observe': event.has_tag('OBSERVE'),
            'ACQUIRE_DRM1_1F': event.transition_index[0] == 101,
            'LOCKING_ALS': event.transition_index[0] == 15,
        },
        'L1': {
            'Observe': event.has_tag('OBSERVE'),
        },
    }
    if os.path.exists(sat_path):
        sats = get_five_sats(sat_path)
        five_sats.append(sats)
    for key, condition in sat_conditions[config.IFO].items():
        if condition:
            if os.path.exists(sat_path):
                saturations[config.IFO][key][sats[0]] += 1
            else:
                saturations[config.IFO][key]['No saturations'] += 1

    return saturations, five_sats


def get_five_sats(sat_path):
    """Returns shortened names of first five channels located at sat_path.

    Returns list containing up to the first five saturations in a given
    lockloss. Locklosses with no saturations will not be called due to logic
    in check_saturations.
    """
    all_sats = np.genfromtxt(
        sat_path,
        delimiter=' ',
        dtype=str,
        usecols=0,
    )
    five_sats = np.array([])
    all_sats = np.array(all_sats, ndmin=1)
    for sat in all_sats:
        # create shortened channel name (excluding IFO, quadrant, characters)
        sat_123 = sat.split('-')
        sat1 = sat_123[0]
        sat2 = sat_123[1].split('_')[0]
        sat3 = sat_123[1].split('_')[1]
        channel_shorthand = '%s %s' % (sat2, sat3)
        # Make sure the degenerate channels don't get added
        if channel_shorthand not in five_sats:
            five_sats = np.append(five_sats, channel_shorthand)
            if len(five_sats) == 5:
                break

    return five_sats


def plot_summary(path, epoch):
    """Plots lockloss summary data and saves to example_plots.

    Plots histograms for lockloss state transitions, time lengths in Observing,
    and first saturating suspension channel for the three most common lockloss
    states. Saves these to the example_plots directory.
    """
    epoch_path = os.path.join(path, epoch)
    try:
        os.makedirs(epoch_path)
    except FileExistsError:
        pass

    transitions, observe_durations, saturations, five_sats, shifts, tag_count = grab_data(EPOCHS[epoch])

    plotutils.set_rcparams()

    # Transition state plot
    state_position = np.arange(len(transitions))
    sort_keys = list(transitions.keys())
    sort_keys.sort()
    sort_values = [transitions[x] for x in sort_keys if bool(sort_keys)]
    fig, ax = plt.subplots(1, figsize=(22,16))
    ax.bar(
        state_position,
        sort_values,
        align='center',
    )
    ax.set_xlabel('State from which lockloss has occurred', labelpad=10)
    ax.set_ylabel('Number of locklosses')
    ax.set_title('O3 lockloss occurences by final state: %s' % (epoch))
    ax.set_xticks(state_position)
    ax.tick_params(axis='x', which='major', labelsize=18)
    ax.set_xticklabels(sort_keys, rotation=45, ha='right')
    ax.set_xlim([-1, state_position.size])
    plt.gcf().text(0.02, 0.02, "Created: {}".format(gpsnow()), fontsize=16)
    fig.tight_layout()

    outpath_plot = os.path.join(epoch_path, 'Lockloss_states')
    fig.savefig(outpath_plot, bbox_inches='tight')
    plt.close()

    # Lock duration plot
    fig, ax = plt.subplots(1, figsize=(22, 16))
    if epoch == 'run':
        bin_num = 30
    if epoch == 'month':
        bin_num = 10
    if epoch == 'week':
        bin_num = 5
    fig, ax = plt.subplots(1, figsize=(22, 16))
    ax.hist(
        observe_durations,
        bins=bin_num,
        align='mid',
    )
    ax.set_xlabel('Lock duration [hours]', labelpad=10)
    ax.set_ylabel('Number of locks')
    ax.set_title('Observe lock durations: %s' % (epoch))
    plt.gcf().text(0.02, 0.02, "Created: {}".format(gpsnow()), fontsize=16)
    fig.tight_layout()

    outpath_plot = os.path.join(epoch_path, 'Lock_durations')
    fig.savefig(outpath_plot, bbox_inches='tight')
    plt.close()

    # Saturating suspension channel plot
    for state, sat_dict in saturations[config.IFO].items():
        sat_position = np.arange(len(sat_dict))
        sort_keys = list(sat_dict.keys())
        sort_keys.sort()
        sort_values = [sat_dict[x] for x in sort_keys if bool(sort_keys)]
        fig, ax = plt.subplots(1, figsize=(22,16))
        ax.bar(
            sat_position,
            sort_values,
            align='center',
        )
        ax.set_xlabel('First suspension to saturate before lockloss', labelpad=10)
        ax.set_ylabel('Number of locklosses')
        ax.set_title('%s locklosses by saturating suspension: %s' % (state, epoch))
        ax.set_xticks(sat_position)
        ax.set_xticklabels(sort_keys, rotation=45, ha='right')
        ax.set_xlim([-1, sat_position.size])
        plt.gcf().text(0.02, 0.02, "Created: {}".format(gpsnow()), fontsize=16)
        fig.tight_layout()

        plot_name = '%s_lockloss_saturations' % (state)
        outpath_plot = os.path.join(epoch_path, plot_name)
        fig.savefig(outpath_plot, bbox_inches='tight')
        plt.close()

    # Lockloss shift plot
    counts = [x['counts'] for x in shifts.values()]
    shifts = shifts.keys()
    fig, ax = plt.subplots(1, figsize=(22, 16))
    shift_x = np.array([0, 1, 2])
    ax.bar(
        shift_x,
        counts,
        align='center',
    )
    ax.set_xlabel('Operating shift', labelpad=10)
    ax.set_ylabel('Number of locklosses')
    ax.set_title('Number of locklosses per shift: %s' % (epoch))
    ax.set_xticks(shift_x)
    ax.set_xticklabels(shifts, rotation=45, ha='right')
    ax.set_xlim([-1, shift_x.size])
    plt.gcf().text(0.02, 0.02, "Created: {}".format(gpsnow()), fontsize=16)
    fig.tight_layout()

    outpath_plot = os.path.join(epoch_path, 'Lockloss_by_shift')
    fig.savefig(outpath_plot, bbox_inches='tight')
    plt.close()

    # Associated tag plot
    fig, ax = plt.subplots(1, figsize=(22,16))
    shift_x = np.arange(0, len(tag_count))
    ax.bar(
        shift_x,
        tag_count.values(),
        align='center',
    )
    ax.set_xlabel('Lockloss tags', labelpad=10)
    ax.set_ylabel('Number of locklosses')
    ax.set_title('Number of locklosses with given tag: %s' % (epoch))
    ax.set_xticks(shift_x)
    ax.set_xticklabels(tag_count.keys(), rotation=45, ha='right')
    ax.set_xlim([-1, shift_x.size])
    plt.gcf().text(0.02, 0.02, "Created: {}".format(gpsnow()), fontsize=16)
    fig.tight_layout()

    outpath_plot = os.path.join(epoch_path, 'Lockloss_by_tag')
    fig.savefig(outpath_plot, bbox_inches='tight')
    plt.close()

    #saturating suspension grid plot
    y_sats = sorted(list(set([i[0] for i in five_sats])))
    x_sats = sorted(list(set([item for sublist in five_sats for item in sublist[1:]])))
    sat_grid = np.zeros((len(y_sats), len(x_sats)))
    for y_idx, yval in enumerate(y_sats):
        for x_idx, xval in enumerate(x_sats):
            for sat_set in five_sats:
                if yval == sat_set[0]:
                    if xval in sat_set[1:]:
                        sat_grid[y_idx][x_idx] += 1

    fig, ax = plt.subplots(1, figsize=(22,16))
    img = ax.imshow(sat_grid, interpolation='nearest',)
    ax.set_xlabel('2nd-5th saturating suspension', labelpad=10)
    ax.tick_params(width=5)
    ax.set_ylabel('First saturating suspension')
    ax.set_title('O3 suspension correlations: %s' % (epoch))
    ax.set_xticks(np.arange(len(x_sats)))
    ax.set_yticks(np.arange(len(y_sats)))
    ax.set_xticklabels(x_sats, rotation = 45, ha = 'right')
    ax.set_yticklabels(y_sats)
    fig.tight_layout()
    cbar = fig.colorbar(img)
    cbar.set_label('# of locklosses')

    outpath_plot = os.path.join(epoch_path, 'suspension_grid')
    fig.savefig(outpath_plot, bbox_inches='tight')
    plt.close()

######################################################

def _parser_add_arguments(parser):
    parser.add_argument(
        '--path', '-p', default=config.SUMMARY_ROOT, type=str,
        help="summary plots directory")
    parser.add_argument(
        'epoch', nargs='*', default=list(EPOCHS),
        help="epoch to calculate {}, default all".format(list(EPOCHS)))


def main(args=None):
    """Generate lockloss summary plots."""
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    for epoch in args.epoch:
        if epoch not in set(EPOCHS):
            msg = "'{}' is not a valid epoch.".format(epoch)
            try:
                parser.error(msg)
            except UnboundLocalError:
                raise SystemExit(msg)

    try:
        os.mkdir(args.path)
    except FileExistsError:
        pass

    for epoch in args.epoch:
        print('summarizing locklosses epoch {}'.format(epoch))
        plot_summary(args.path, epoch)


if __name__ == '__main__':
    main()
