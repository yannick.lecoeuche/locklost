import io
import sys
import logging
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import xml.etree.ElementTree as ET

import gpstime
from gwpy.segments import DataQualityFlag

from . import config
from . import data
from .event import find_events

# plt.rc('text', usetex=True)

##################################################

def gtmin(gt):
    """GPS time rounded to minute from gpstime object"""
    return int(gt.gps() / 60) * 60


def gps2dn(gps):
    """convert GPS to matplotlib date num

    dayes since 0001-01-01:

    https://matplotlib.org/3.1.1/api/dates_api.html#matplotlib.dates.date2num

    """
    return dates.date2num(gpstime.parse(gps))


def event_dn(e):
    """return event matplotlib date num value"""
    return gps2dn(e.gps)


def escatter(events, **kwargs):
    """scatter plot of events"""
    x = [event_dn(e) for e in events]
    y = [e.transition_index[0] for e in events]
    urls = [e.view_url for e in events]
    s = plt.scatter(
        x, y,
        linewidth=0,
        **kwargs
    )
    s.set_urls(urls)


def gps2dnum(gps_array):
    """convert array of equal-spaced GPS seconds to matplotlib date num"""
    gps = gps_array[0]
    return gps2dn(gps) + (gps_array - gps) / 3600 / 24


def plot_history(path, lookback='7 days ago', draw_segs=False):
    """Plot interactive history of events

    """
    start = gpstime.parse(lookback)
    end = gpstime.parse('now')

    # retrieve guardian state data
    channel = config.GRD_STATE_N_CHANNEL
    dt = end - start
    if dt.total_seconds() >= 36000:
        channel += '.mean,m-trend'
    else:
        channel += '.mean,s-trend'
    bufs = data.nds_fetch([channel], gtmin(start), gtmin(end))
    state_index, state_time = bufs[0].yt()
    if np.all(np.isnan(state_index)):
        logging.warning("state data [{}] is all nan??".format(channel))

    # find events
    events = list(find_events(after=start.gps()))

    fig = plt.figure(figsize=(16, 4)) #, dpi=80)
    ax = fig.add_subplot(111)

    # plot analyzing
    escatter(
        [e for e in events if e.analyzing],
        s=100, color='orange',
    )

    # plot failed lock losses
    escatter(
        [e for e in events if not e.analyzing and not e.analysis_succeeded],
        s=100, color='red',
    )

    # plot non-observe lock losses
    escatter(
        [e for e in events if e.analysis_succeeded and not e.has_tag('OBSERVE')],
        s=100, color='blue',
    )

    # plot observe lock losses (stars)
    escatter(
        [e for e in events if e.analysis_succeeded and e.has_tag('OBSERVE')],
        s=200, marker='*', color='green',
    )

    plt.plot(
        gps2dnum(state_time),
        state_index,
        color='orange', linewidth=1, zorder=-100,
    )

    # find/draw science segments
    if draw_segs:
        flag = '{}:DMT-SCIENCE:1'.format(config.IFO.upper())
        segs = DataQualityFlag.query(flag, int(start.gps()), int(end.gps()))
        science_segs = segs.active.coalesce()
        for seg in science_segs:
            ss, se = (dates.date2num(gpstime.parse(t)) for t in seg)
            ax.axvspan(ss, se, alpha=0.1, color='green')

    # plot lock segments
    for event in events:
        try:
            seg = (event.previous_state['start'], event.previous_state['end'])
        except TypeError:
            continue
        ss, se = (dates.date2num(gpstime.parse(t)) for t in seg)
        # if event.has_tag('OBSERVE'):
        #     color = 'green'
        # else:
        #     color = 'blue'
        color = 'green'
        ax.axvspan(ss, se, alpha=0.1, color=color)

    # now = dates.date2num(gpstime.parse('now'))
    # ax.axvline(now, color='red')
    ax.set_xlim((dates.date2num(dt) for dt in [start, end]))

    # state_set = set([e.transition_index[0] for e in events])
    # ticks = []
    # last = -np.inf
    # for s in sorted(state_set):
    #     if s < last + 50:
    #         continue
    #     ticks.append(s)
    #     last = s
    # ax.set_yticks(ticks)
    # ylim = ax.get_ylim()
    # ax.set_ylim((-10, ylim[1]))
    # ax.set_ylim(min(state_set)-20, max(state_set)+20)

    fig.autofmt_xdate()
    xfmt = dates.DateFormatter('%a %Y-%m-%d')
    ax.xaxis.set_major_formatter(xfmt)
    # ax.xaxis.set_major_locator(dates.DayLocator())

    # plt.suptitle("recent lock losses")
    plt.ylabel("ISC_LOCK state")
    # plt.xlabel("UTC time")
    plt.grid(True)

    # plt.savefig(path, bbox_inches='tight')

    # save SVG, but modify to add 'target' attributes to all '<a
    # href>' tags, to specify that the links open at the top of the
    # window, and not in the html object
    # https://stackoverflow.com/questions/7008355/how-to-make-links-in-an-embedded-svg-file-open-in-the-main-window-not-in-a-sepa#7017568
    svg = io.BytesIO()
    plt.savefig(svg, format="svg", bbox_inches='tight')
    svg.seek(0)
    tree = ET.parse(svg)
    root = tree.getroot()
    for el in root.iter('{http://www.w3.org/2000/svg}a'):
        # https://html.spec.whatwg.org/multipage/browsers.html#valid-browsing-context-name-or-keyword
        el.set('target', '_top')
    tree.write(path)

##################################################

def main():
    path = sys.argv[1]
    plot_history(path)


if __name__ == '__main__':
    main()
