import os
import sys
from collections import OrderedDict

IFO = os.getenv('IFO')
if not IFO:
    sys.exit('Must specify IFO env var.')

def ifochans(channels):
    return ['{}:{}'.format(IFO, chan) for chan in channels]

LOG_FMT = '%(asctime)s [%(module)s.%(funcName)s] %(message)s'

EVENT_ROOT = os.getenv('LOCKLOST_EVENT_ROOT', '')
if os.path.exists(EVENT_ROOT):
    EVENT_ROOT = os.path.abspath(EVENT_ROOT)
SUMMARY_ROOT = os.getenv('LOCKLOST_SUMMARY_ROOT', '')
WEB_ROOT = os.getenv('LOCKLOST_WEB_ROOT', '')

SEG_DIR = os.path.join(EVENT_ROOT, '.segments')

CONDOR_ONLINE_DIR = os.path.join(EVENT_ROOT, '.condor_online')
CONDOR_SEARCH_DIR = os.path.join(EVENT_ROOT, '.condor_search')
CONDOR_ANALYZE_DIR = os.path.join(EVENT_ROOT, '.condor_analyze')

QUERY_DEFAULT_LIMIT = 50

CONDOR_ACCOUNTING_GROUP = os.getenv('CONDOR_ACCOUNTING_GROUP')
CONDOR_ACCOUNTING_GROUP_USER = os.getenv('CONDOR_ACCOUNTING_GROUP_USER')
CONDOR_KILL_SIGNAL = 'SIGTERM'

GRD_NODE = 'ISC_LOCK'
GRD_STATE_N_CHANNEL = '{}:GRD-{}_STATE_N'.format(IFO, GRD_NODE)
GRD_STATE_S_CHANNEL = '{}:GRD-{}_STATE_S'.format(IFO, GRD_NODE)

CA_MONITOR_CHANNEL = GRD_STATE_S_CHANNEL

if IFO == 'H1':
    GRD_NOMINAL_STATE = (600, 'NOMINAL_LOW_NOISE')
elif IFO == 'L1':
    GRD_NOMINAL_STATE = (2000, 'LOW_NOISE')
GRD_LOCKLOSS_STATES = [
   (2, 'LOCKLOSS'),
    # (3, 'LOCKLOSS_DRMI'),
]

if IFO == 'H1':
    CDS_EVENT_ROOT = 'https://lhocds.ligo-wa.caltech.edu/exports/lockloss/events'
elif IFO == 'L1':
    CDS_EVENT_ROOT = 'https://llocds.ligo-la.caltech.edu/data/lockloss/events'

O3_GPS_START = 1238112018

SEARCH_STRIDE = 10000

DATA_ACCESS = os.getenv('DATA_ACCESS', 'gwpy')

MAX_QUERY_LATENCY = 70

DATA_DISCOVERY_SLEEP = 30
DATA_DISCOVERY_TIMEOUT = 600

# tag colors for web (foreground, background)
TAG_COLORS = {
    'OBSERVE': ('black', 'lime'),
    'REFINED': ('white', 'grey'),
    'BRS_GLITCH': ('rebeccapurple', 'lavender'),
    'BOARD_SAT': ('navy', 'cornflowerblue'),
    'WINDY': ('springgreen', 'mediumblue'),
    'EARTHQUAKE': ('orange', 'black'),
    'MICROSEISMIC': ('yellow', 'black'),
    'ANTHROPOGENIC': ('red', 'black'),
    'ADS_EXCURSION': ('mediumvioletred', 'plum'),
    'FSS_OSCILLATION': ('69650b', 'palegoldenrod'),
    'SEI_BS_TRANS': ('sienna', 'orange'),
    'COMMISSIONING': ('lightseagreen', 'darkslategrey'),
    'MAINTENANCE': ('lightseagreen', 'darkslategrey'),
    'CALIBRATION': ('lightseagreen', 'darkslategrey'),
    'FAST_DRMI': ('blue', 'pink'),
}

##################################################

PLOT_WINDOWS = {
    'WIDE': [-30, 10],
    'ZOOM': [-5, 1],
}

FIG_SIZE = [n*0.6 for n in [16, 9]]

##################################################

REFINE_WINDOW = [-20, 5]
REFINE_PLOT_WINDOWS = {
    'WIDE': REFINE_WINDOW,
    'ZOOM': [-5, 1],
}

# per IFO key is the state index threshold for the specified indicator
# params, e.g. for H1, for lock losses from states between 102 and 410
# the indicator parameters will be...
# CHANNEL: indicator channel
# THRESHOLD: % stddev relative to mean (e.g. "-10" is "less than 10*stdev below mean")
# MINIMUM: minimum mean value for which indicator is valid
if IFO == 'H1':
    INDICATORS = {
        410: {
            'CHANNEL': 'H1:ASC-AS_A_DC_NSUM_OUT_DQ',
            'THRESHOLD': 10,
            'MINIMUM': 50,
        },
        102: {
            'CHANNEL': 'H1:LSC-POPAIR_B_RF18_I_ERR_DQ',
            'THRESHOLD': -10,
            'MINIMUM': 50,
        },
        0: {
            'CHANNEL': 'H1:IMC-TRANS_OUT_DQ',
            'THRESHOLD': -10,
            'MINIMUM': 50,
        },
    }
elif IFO == 'L1':
    INDICATORS = {
        0: {
            'CHANNEL': 'L1:IMC-TRANS_OUT_DQ',
            'THRESHOLD': -100,
            'MINIMUM': 50,
        },
    }

##################################################

PLOT_SATURATIONS = 8
SATURATION_THRESHOLD = 131072
PLOT_CHUNKSIZE = 3000

# This corresponds to a change from 18 to 20-bit DAC for ETMX L3 channels.
# ETMX L3 channels after this date have counts that are four times higher
if IFO == 'H1':
    CHANGE_DAC_DATE = 1224961218
if IFO == 'L1':
    CHANGE_DAC_DATE = 1188518418

# ETM's are 20-bit and were changed at a date within our wanted
# lockloss time range
if IFO == 'H1':
    ETM_L3_CHANNELS = [
        'SUS-ETMX_L3_MASTER_OUT_UR_DQ',
        'SUS-ETMX_L3_MASTER_OUT_UL_DQ',
        'SUS-ETMX_L3_MASTER_OUT_LR_DQ',
        'SUS-ETMX_L3_MASTER_OUT_LL_DQ',
    ]
if IFO == 'L1':
    ETM_L3_CHANNELS = [
        'SUS-ETMY_L3_MASTER_OUT_DC_DQ',
        'SUS-ETMY_L3_MASTER_OUT_UR_DQ',
        'SUS-ETMY_L3_MASTER_OUT_UL_DQ',
        'SUS-ETMY_L3_MASTER_OUT_LR_DQ',
        'SUS-ETMY_L3_MASTER_OUT_LL_DQ'
    ]
ETM_L3_CHANNELS = ifochans(ETM_L3_CHANNELS)

# RM's, OM'S, and ZM'S are 16-bit
SIXTEEN_BIT_CHANNELS = [
    'SUS-RM1_M1_MASTER_OUT_UR_DQ',
    'SUS-RM1_M1_MASTER_OUT_UL_DQ',
    'SUS-RM1_M1_MASTER_OUT_LR_DQ',
    'SUS-RM1_M1_MASTER_OUT_LL_DQ',
    'SUS-RM2_M1_MASTER_OUT_UR_DQ',
    'SUS-RM2_M1_MASTER_OUT_UL_DQ',
    'SUS-RM2_M1_MASTER_OUT_LR_DQ',
    'SUS-RM2_M1_MASTER_OUT_LL_DQ',
    'SUS-ZM1_M1_MASTER_OUT_UR_DQ',
    'SUS-ZM1_M1_MASTER_OUT_UL_DQ',
    'SUS-ZM1_M1_MASTER_OUT_LR_DQ',
    'SUS-ZM1_M1_MASTER_OUT_LL_DQ',
    'SUS-ZM2_M1_MASTER_OUT_UR_DQ',
    'SUS-ZM2_M1_MASTER_OUT_UL_DQ',
    'SUS-ZM2_M1_MASTER_OUT_LR_DQ',
    'SUS-ZM2_M1_MASTER_OUT_LL_DQ',
    'SUS-OM1_M1_MASTER_OUT_UR_DQ',
    'SUS-OM1_M1_MASTER_OUT_UL_DQ',
    'SUS-OM1_M1_MASTER_OUT_LR_DQ',
    'SUS-OM1_M1_MASTER_OUT_LL_DQ',
    'SUS-OM2_M1_MASTER_OUT_UR_DQ',
    'SUS-OM2_M1_MASTER_OUT_UL_DQ',
    'SUS-OM2_M1_MASTER_OUT_LR_DQ',
    'SUS-OM2_M1_MASTER_OUT_LL_DQ',
    'SUS-OM3_M1_MASTER_OUT_UR_DQ',
    'SUS-OM3_M1_MASTER_OUT_UL_DQ',
    'SUS-OM3_M1_MASTER_OUT_LR_DQ',
    'SUS-OM3_M1_MASTER_OUT_LL_DQ',
]
SIXTEEN_BIT_CHANNELS = ifochans(SIXTEEN_BIT_CHANNELS)

ANALOG_BOARD_CHANNELS = [
    'IMC-REFL_SERVO_SPLITMON',
    'IMC-REFL_SERVO_FASTMON',
    'IMC-REFL_SERVO_SUMMON',
    'LSC-REFL_SERVO_SUMMON',
    'LSC-REFL_SERVO_SPLITMON',
    'LSC-REFL_SERVO_FASTMON',
    'LSC-REFL_SERVO_SLOWFBMON',
    'LSC-REFL_SERVO_SLOWMON',
]
ANALOG_BOARD_CHANNELS = ifochans(ANALOG_BOARD_CHANNELS)

BOARD_SEARCH_WINDOW = [-30, 5]

if IFO == 'H1':
    BOARD_SAT_BUFFER = 0
if IFO == 'L1':
    BOARD_SAT_BUFFER = 0.5

BOARD_SAT_THRESH = 9.5

##################################################

ADS_CHANNELS = []
for py in ['PIT', 'YAW']:
    for num in [3, 4, 5]:
        ADS_CHANNELS.append('%s:ASC-ADS_%s%s_DEMOD_I_OUT16' % (IFO, py, num))

ADS_SEARCH_WINDOW = [-40, 5]

ADS_THRESH = 0.1

##################################################

LPY_CHANNELS = [
    'SUS-ETMX_L2_MASTER_OUT_UR_DQ',
    'SUS-ETMX_L2_MASTER_OUT_UL_DQ',
    'SUS-ETMX_L2_MASTER_OUT_LR_DQ',
    'SUS-ETMX_L2_MASTER_OUT_LL_DQ',
]
LPY_CHANNELS = ifochans(LPY_CHANNELS)

##################################################

BOARD_SAT_CM = [
    'mediumseagreen',
    'cornflowerblue',
    'mediumvioletred',
    'orangered',
    'darkorchid',
    'goldenrod',
    'teal',
    'lightcoral',
]
SATURATION_CM = [
    '#332288',
    '#88CCEE',
    '#117733',
    '#999933',
    '#DDCC77',
    '#CC6677',
    '#882255',
    '#AA4499',
]
SC_CM = [
    '#e6194b',
    '#3cb44b',
    '#4363d8',
    '#f58231',
    '#911eb4',
    '#46f0f0',
    '#f032e6',
    '#bcf60c',
    '#008080',
]

ADC_OVERFLOWS = {
    'ASC': {
        'ADC_ID': 19,
        'num_bits': 5,
        'bit_exclude': [],
    },
    'LSC': {
        'ADC_ID': 10,
        'num_bits': 3,
        'bit_exclude': [(2, 4), (2, 13), (2, 15)],
    },
}
if IFO == 'H1':
    ADC_OVERFLOWS['OMC'] = {
        'ADC_ID': 8,
        'num_bits': 3,
        'bit_exclude': [(2, 4), (2, 13), (2, 15)],
    }

SAT_SEARCH_WINDOW = [-30, 5]

##################################################

WIND_SEARCH_WINDOW = [-600, 5]
if IFO == 'H1':
    WIND_CHANNELS = [
        'H1:PEM-EX_WIND_ROOF_WEATHER_MPH',
        'H1:PEM-EY_WIND_ROOF_WEATHER_MPH',
        'H1:PEM-CS_WIND_ROOF_WEATHER_MPH',
    ]
    WIND_THRESH = 20
elif IFO == 'L1':
    WIND_CHANNELS = [
        'L1:PEM-EY_WIND_WEATHER_MPH',
        'L1:PEM-LVEA_WIND_WEATHER_MPH',
    ]
    WIND_THRESH = 20

##################################################

# Set seismic band thresholds based on IFO site
if IFO == 'H1':
    SEI_EARTHQUAKE_THRESH = 300
    SEI_ANTHROPOGENIC_THRESH = 1000 # placeholder
    SEI_MICROSEISMIC_THRESH = 3000 # placeholder
if IFO == 'L1':
    SEI_EARTHQUAKE_THRESH = 600
    SEI_ANTHROPOGENIC_THRESH = 1000
    SEI_MICROSEISMIC_THRESH = 3000

# Main data structure, each subdictionary contains the channel, save
# file name, data quality key, channel's data axis, threshold for the
# channel, and tag designation
SEISMIC_CONFIG = {
    'EQ band': {
        'channel': 'ISI-GND_STS_ITMY_Z_BLRMS_30M_100M',
        'savefile': 'seismic_eq.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_EARTHQUAKE_THRESH,
        'tag': 'EARTHQUAKE',
    },
    'anthropogenic corner': {
        'channel': 'ISI-GND_STS_ITMY_Z_BLRMS_1_3',
        'savefile': 'seismic_anthro_ITMY.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_ANTHROPOGENIC_THRESH,
        'tag': 'ANTHROPOGENIC',
    },
    'anthropogenic Xend': {
        'channel': 'ISI-GND_STS_ETMX_Z_BLRMS_1_3',
        'savefile': 'seismic_anthro_ETMX.png',
        'dq_channel': 'ISI-GND_STS_ETMX_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_ANTHROPOGENIC_THRESH,
        'tag': 'ANTHROPOGENIC',
    },
    'anthropogenic Yend': {
        'channel': 'ISI-GND_STS_ETMY_Z_BLRMS_1_3',
        'savefile': 'seismic_anthro_ETMY.png',
        'dq_channel': 'ISI-GND_STS_ETMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_ANTHROPOGENIC_THRESH,
        'tag': 'ANTHROPOGENIC',
    },
    'micro ITMY Y 100u 300u': {
        'channel': 'ISI-GND_STS_ITMY_Y_BLRMS_100M_300M',
        'savefile': 'seismic_micro_Y_100u_300u.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Y_DQ',
        'axis': 'Y',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
    'micro ITMY Y 300u 1': {
        'channel': 'ISI-GND_STS_ITMY_Y_BLRMS_300M_1',
        'savefile': 'seismic_micro_Y_300u_1.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Y_DQ',
        'axis': 'Y',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
    'micro ITMY Z 100u 300u': {
        'channel': 'ISI-GND_STS_ITMY_Z_BLRMS_100M_300M',
        'savefile': 'seismic_micro_Z_100u_300u.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
    'micro ITMY Z 300u 1': {
        'channel': 'ISI-GND_STS_ITMY_Z_BLRMS_300M_1',
        'savefile': 'seismic_micro_Z_300u_1.png',
        'dq_channel': 'ISI-GND_STS_ITMY_Z_DQ',
        'axis': 'Z',
        'threshold': SEI_MICROSEISMIC_THRESH,
        'tag': 'MICROSEISMIC',
    },
}

# Loop to convert channel names to the IFO channel names
# Could not use ifochans function since ifochans was only pulling one character at a time from channel string
for i in SEISMIC_CONFIG:
    SEISMIC_CONFIG[i]['channel'] = '{}:{}'.format(IFO, SEISMIC_CONFIG[i]['channel'])
    SEISMIC_CONFIG[i]['dq_channel'] = '{}:{}'.format(IFO, SEISMIC_CONFIG[i]['dq_channel'])

SEI_SEARCH_WINDOW = [-30, 150]

##################################################

GSTLAL_TRIGGER_DIR = '/home/idq/gstlal/online/features'

GLITCH_PLOT_WINDOWS = {
    'WIDE': [-20, 2],
    'ZOOM': [-5, 1],
}

GLITCH_CHANNELS = {
    'ASC_AS': [
        'ASC-AS_A_DC_NSUM_OUT_DQ',
        'ASC-AS_A_DC_PIT_OUT_DQ',
        'ASC-AS_A_DC_YAW_OUT_DQ',
        'ASC-AS_A_RF36_I_PIT_OUT_DQ',
        'ASC-AS_A_RF36_I_YAW_OUT_DQ',
        'ASC-AS_A_RF36_Q_PIT_OUT_DQ',
        'ASC-AS_A_RF36_Q_YAW_OUT_DQ',
        'ASC-AS_A_RF45_I_PIT_OUT_DQ',
        'ASC-AS_A_RF45_I_YAW_OUT_DQ',
        'ASC-AS_A_RF45_Q_PIT_OUT_DQ',
        'ASC-AS_A_RF45_Q_YAW_OUT_DQ',
        'ASC-AS_B_RF36_I_PIT_OUT_DQ',
        'ASC-AS_B_RF36_I_YAW_OUT_DQ',
        'ASC-AS_B_RF36_Q_PIT_OUT_DQ',
        'ASC-AS_B_RF36_Q_YAW_OUT_DQ',
        'ASC-AS_B_RF45_I_PIT_OUT_DQ',
        'ASC-AS_B_RF45_I_YAW_OUT_DQ',
        'ASC-AS_B_RF45_Q_PIT_OUT_DQ',
        'ASC-AS_B_RF45_Q_YAW_OUT_DQ',
    ],
    'ASC_HS': [
        'ASC-CHARD_Y_OUT_DQ',
        'ASC-CSOFT_P_OUT_DQ',
        'ASC-CSOFT_Y_OUT_DQ',
        'ASC-DHARD_P_OUT_DQ',
        'ASC-DHARD_Y_OUT_DQ',
        'ASC-DSOFT_P_OUT_DQ',
        'ASC-DSOFT_Y_OUT_DQ',
        'ASC-MICH_P_OUT_DQ',
        'ASC-MICH_Y_OUT_DQ',
        'ASC-PRC1_Y_OUT_DQ',
        'ASC-PRC2_P_OUT_DQ',
    ],
    'ASC_REFL': [
        'ASC-REFL_A_RF45_I_PIT_OUT_DQ',
        'ASC-REFL_A_RF45_I_YAW_OUT_DQ',
        'ASC-REFL_A_RF45_Q_PIT_OUT_DQ',
        'ASC-REFL_A_RF45_Q_YAW_OUT_DQ',
        'ASC-REFL_A_RF9_I_PIT_OUT_DQ',
        'ASC-REFL_A_RF9_Q_PIT_OUT_DQ',
        'ASC-REFL_A_RF9_Q_YAW_OUT_DQ',
        'ASC-REFL_B_DC_NSUM_OUT_DQ',
        'ASC-REFL_B_DC_PIT_OUT_DQ',
        'ASC-REFL_B_DC_YAW_OUT_DQ',
        'ASC-REFL_B_RF45_I_PIT_OUT_DQ',
        'ASC-REFL_B_RF45_I_YAW_OUT_DQ',
        'ASC-REFL_B_RF45_Q_PIT_OUT_DQ',
        'ASC-REFL_B_RF45_Q_YAW_OUT_DQ',
        'ASC-REFL_B_RF9_I_PIT_OUT_DQ',
        'ASC-REFL_B_RF9_Q_PIT_OUT_DQ',
        'ASC-REFL_B_RF9_Q_YAW_OUT_DQ',
        'ASC-SRC2_P_OUT_DQ',
        'ASC-SRC2_Y_OUT_DQ',
    ],
    'ASC_TR': [
        'ASC-X_TR_A_NSUM_OUT_DQ',
        'ASC-X_TR_A_PIT_OUT_DQ',
        'ASC-X_TR_A_YAW_OUT_DQ',
        'ASC-X_TR_B_NSUM_OUT_DQ',
        'ASC-X_TR_B_PIT_OUT_DQ',
        'ASC-X_TR_B_YAW_OUT_DQ',
        'ASC-Y_TR_A_NSUM_OUT_DQ',
        'ASC-Y_TR_A_PIT_OUT_DQ',
        'ASC-Y_TR_A_YAW_OUT_DQ',
        'ASC-Y_TR_B_NSUM_OUT_DQ',
        'ASC-Y_TR_B_PIT_OUT_DQ',
        'ASC-Y_TR_B_YAW_OUT_DQ',
    ],
    'IMC': [
        'IMC-DOF_1_P_IN1_DQ',
        'IMC-DOF_2_P_IN1_DQ',
        'IMC-DOF_4_P_IN1_DQ',
        'IMC-F_OUT_DQ',
        'IMC-IM4_TRANS_PIT_OUT_DQ',
        'IMC-IM4_TRANS_SUM_IN1_DQ',
        'IMC-IM4_TRANS_YAW_OUT_DQ',
        'IMC-MC2_TRANS_PIT_OUT_DQ',
        'IMC-PWR_IN_OUT_DQ',
        'IMC-REFL_DC_OUT_DQ',
        'IMC-TRANS_OUT_DQ',
        'IMC-WFS_A_DC_SUM_OUT_DQ',
        'IMC-WFS_A_I_PIT_OUT_DQ',
        'IMC-WFS_A_I_YAW_OUT_DQ',
        'IMC-WFS_A_Q_PIT_OUT_DQ',
        'IMC-WFS_A_Q_YAW_OUT_DQ',
        'IMC-WFS_B_DC_PIT_OUT_DQ',
        'IMC-WFS_B_DC_YAW_OUT_DQ',
        'IMC-WFS_B_I_PIT_OUT_DQ',
        'IMC-WFS_B_Q_PIT_OUT_DQ',
        'IMC-WFS_B_Q_YAW_OUT_DQ',
    ],
    'LSC': [
        'LSC-MCL_IN1_DQ',
        'LSC-MICH_IN1_DQ',
        'LSC-MICH_OUT_DQ',
        'LSC-MOD_RF45_AM_AC_OUT_DQ',
        'LSC-POPAIR_A_RF9_Q_ERR_DQ',
        'LSC-POP_A_LF_OUT_DQ',
        'LSC-POP_A_RF45_I_ERR_DQ',
        'LSC-POP_A_RF9_Q_ERR_DQ',
        'LSC-PRCL_IN1_DQ',
        'LSC-PRCL_OUT_DQ',
        'LSC-REFL_A_LF_OUT_DQ',
        'LSC-REFL_A_RF45_I_ERR_DQ',
        'LSC-REFL_A_RF45_Q_ERR_DQ',
        'LSC-REFL_A_RF9_Q_ERR_DQ',
        'LSC-REFL_SERVO_ERR_OUT_DQ',
        'LSC-Y_ARM_OUT_DQ',
    ],
    'PSL': [
        'PSL-FSS_PC_MON_OUT_DQ',
        'PSL-ILS_HV_MON_OUT_DQ',
        'PSL-ILS_MIXER_OUT_DQ',
        'PSL-ISS_PDA_REL_OUT_DQ',
        'PSL-ISS_PDB_REL_OUT_DQ',
        'PSL-ISS_SECONDLOOP_QPD_PIT_OUT_DQ',
        'PSL-OSC_PD_AMP_DC_OUT_DQ',
        'PSL-OSC_PD_INT_DC_OUT_DQ',
        'PSL-OSC_PD_ISO_DC_OUT_DQ',
        'PSL-PMC_HV_MON_OUT_DQ',
        'PSL-PMC_MIXER_OUT_DQ',
    ],
}

##################################################

LSC_ASC_CHANNELS = OrderedDict()
LSC_ASC_CHANNELS['Power Buildups'] = [
    'LSC-POPAIR_B_RF18_I_ERR_DQ',
    'LSC-POPAIR_B_RF90_I_ERR_DQ',
    'LSC-REFL_A_LF_OUT_DQ',
    'LSC-POP_A_LF_OUT_DQ',
    # 'ASC-X_PWR_CIRC_OUT16',
    # 'ASC-Y_PWR_CIRC_OUT16',
]
LSC_ASC_CHANNELS['LSC Control Signals'] = [
    'LSC-MICH_OUT_DQ',
    'LSC-PRCL_OUT_DQ',
    'LSC-SRCL_OUT_DQ',
    'LSC-DARM_OUT_DQ',
    # 'LSC-MICHFF_OUT_DQ',
    # 'LSC-SRCLFF1_OUT_DQ',
    'LSC-MCL_OUT_DQ',
    'LSC-REFL_SERVO_CTRL_OUT_DQ',
    'IMC-L_OUT_DQ',
    'IMC-F_OUT_DQ',
]
LSC_ASC_CHANNELS['LSC Sensors'] = [
    'LSC-POP_A_RF9_I_ERR_DQ',
    'LSC-POP_A_RF9_Q_ERR_DQ',
    'LSC-REFL_A_RF9_I_ERR_DQ',
    'LSC-REFL_A_RF9_Q_ERR_DQ',
    'LSC-POP_A_RF45_I_ERR_DQ',
]
LSC_ASC_CHANNELS['ASC Control Signals (Arm)'] = [
    'ASC-CHARD_Y_OUT_DQ',
    'ASC-CHARD_P_OUT_DQ',
    'ASC-DHARD_Y_OUT_DQ',
    'ASC-DHARD_P_OUT_DQ',
    'ASC-CSOFT_Y_OUT_DQ',
    'ASC-CSOFT_P_OUT_DQ',
    'ASC-DSOFT_Y_OUT_DQ',
    'ASC-DSOFT_P_OUT_DQ',
]
LSC_ASC_CHANNELS['ASC Control Signals (Vertex)'] = [
    'ASC-PRC1_Y_OUT_DQ',
    'ASC-PRC1_P_OUT_DQ',
    'ASC-PRC2_Y_OUT_DQ',
    'ASC-PRC2_P_OUT_DQ',
    'ASC-MICH_Y_OUT_DQ',
    'ASC-MICH_P_OUT_DQ',
    'ASC-SRC1_Y_OUT_DQ',
    'ASC-SRC1_P_OUT_DQ',
    'ASC-SRC2_Y_OUT_DQ',
    'ASC-SRC2_P_OUT_DQ',
    'ASC-INP1_P_OUT_DQ',
    'ASC-INP1_Y_OUT_DQ',
]
LSC_ASC_CHANNELS['ASC Centering Control Signals'] = [
    'ASC-DC1_Y_OUT_DQ',
    'ASC-DC1_P_OUT_DQ',
    'ASC-DC2_Y_OUT_DQ',
    'ASC-DC2_P_OUT_DQ',
    'ASC-DC3_Y_OUT_DQ',
    'ASC-DC3_P_OUT_DQ',
    'ASC-DC4_Y_OUT_DQ',
    'ASC-DC4_P_OUT_DQ',
]
for key in LSC_ASC_CHANNELS:
    LSC_ASC_CHANNELS[key] = ifochans(LSC_ASC_CHANNELS[key])
