% rebase('base.tpl', IFO=IFO, web_script=web_script, online_status=online_status, is_home=is_home)

% from datetime import timedelta
% from locklost.web import utils

<!-- event form -->
<div class="container">
<form method="get" action="{{web_script}}">
 <div class="row">
 show event:
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="GPS time" name="event">
   </div>
 </div>
</form>
</div>

<!-- event filter form -->
<br />
<div class="container">
<form method="get" action="{{web_script}}">
 <div class="row">
 filter events:
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="after GPS" name="after">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="before GPS" name="before">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="guardian state" name="state">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="tag" name="tag">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="# events to display" name="limit">
   </div>
   <div class="col-sm-2">
     <button type="submit" class="btn btn-primary mb-2">Submit</button>
   </div>
 </div>
</form>
</div>

<!-- query info -->
% query_info = ', '.join(['{}={}'.format(k, v) for k, v in query.items()])
<h4>query: {{query_info}}</h4>

<!-- event table -->
<div class="container">
<div class="col-md-12">
<table class="table table-condensed table-hover">
<thead>
<tr>
<th></th>
<th>ID</th>
<th>GPS</th>
<th>UTC</th>
<th>guardian state</th>
<th>state duration</th>
<th>tags</th>
<th>analysis status</th>
</tr>
</thead>
<tbody>

% for ii, event in enumerate(utils.event_gen(query)):
%     tags = utils.tag_buttons(event.list_tags())
%     status_button = utils.analysis_status_button(event)
%     previous_state = event.previous_state
%     if previous_state:
%         index = previous_state['index']
%         duration = str(timedelta(seconds=int(previous_state['end'] - previous_state['start'])))
%     else:
%         index = event.transition_index[0]
%         duration = ''
%     end
      <!-- <tr class='clickable-row' data-href='{{web_script}}/event/{{event.id}}' style="cursor: pointer;"> -->
      <tr>
      <div class="btn-group">
      <td><span style="color:grey">{{ii}}</span></td>
      <td>{{event.id}}</td>
      <td><a href={{web_script}}/event/{{event.id}}>{{event.gps}}</a></td>
      <td><a href={{web_script}}/event/{{event.id}}>{{event.utc}}</a></td>
      <td>{{index}}</td>
      <td>{{duration}}</td>
      <td>{{!tags}}</td>
      <td>{{!status_button}}</td>
      </div>
      </tr>
% end

</tbody>
</table>
</div>
</div>
