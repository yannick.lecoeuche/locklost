% rebase('base.tpl', IFO=IFO, web_script=web_script, online_status=online_status, is_home=is_home)

% import os
% from datetime import timedelta
% from locklost import config
% from locklost.web.utils import analysis_status_button, event_plot_urls
% from locklost.plugins import brs

<!-- event status button -->
% status_button = analysis_status_button(event)
{{!status_button}}

<!-- event overview -->
% state_index = event.transition_index[0]
% if event.previous_state:
%     duration = str(timedelta(seconds=int(event.previous_state['end'] - event.previous_state['start'])))
% else:
%     duration = '???'
% end
% url = event.url()
% log_url = os.path.join(config.CDS_EVENT_ROOT, event.epath, 'guardlog.txt')
<h3>lockloss: <a href="{{url}}">{{event.id}}</a></h3>
<h5>GPS {{event.gps}}</h5>
<h5>{{event.utc}}</h5>
<h6>guardian state: <tt>{{config.GRD_NODE}}:<span style="color:green">{{state_index}}</span></tt>,  duration: <span style="color:green">{{duration}}</span></h6>
<h6><a href="{{log_url}}">guardian lock loss log</a></h6>
<h6>tags: {{!tags}}</h6>

<!-- refined gps -->
<hr />
<div class="container">
<div class="row">
% include('plots.tpl', plots=event_plot_urls(event, 'indicators'), size=6)
</div>
</div>

<!-- saturation plots -->
% if sat_channels:
      <hr />
      <h3>Saturation Plots</h3>
      <br />
      <div class="row">
%     include('plots.tpl', plots=event_plot_urls(event, 'saturations'), size=6)
      </div>

      <!-- saturation table -->
      <div class="panel-group">
      <div class="panel panel-default">
      <div class="panel-heading">
      <h5 class="panel-title"><a data-toggle="collapse" href="#sat1">Saturations table by channel (click to show)</a></h5>
      </div>
      <div id="sat1" class="panel-collapse collapse">
      <div class="panel-body">
      <table class="table table-condensed table-hover">

      <tr>
      <th>Channel</th>
      <th>Time to first saturation</th>
      </tr>

      % for channel, time in sat_channels:
            <tr>
            <div class="row">
            <td>{{channel}}</td>
            <td>{{time}}</td>
            </div>
            </tr>
      %  end

      </table>
      </div>
      </div>
      </div>
      </div>

% else:
      <p>No saturating suspension channels before refined lockloss time.</p>
% end

<!-- LPY plots -->
% if sat_channels:
      <hr />
      <div class="container">
      <div class="row">
      <h3>Length-Pitch-Yaw Plots</h3>
      <br />
      <p>Length, pitch, and yaw drives reconstructed from osem DAC counts for first suspension stage to saturate.</p>
%     include('plots.tpl', plots=event_plot_urls(event, 'lpy'), size=6)
      </div>
      </div>
% else:
      <p>LPY plots not created due to lack of saturating suspension channels.</p>
% end

<!-- FSS oscillations -->
<hr />
<div class="container">
<br />
<div class="row">
% has_tag = event.has_tag('FSS_OSCILLATION')
% board_plot = [event.url('fss.png')]
% include('collapsed_plots.tpl', title ='FSS oscillation plot', id='fss', plots=board_plot, size=5, expand=has_tag, section='main')
</div>
</div>

<!-- BRS glitch -->
<hr />
<div class="container">
<br />
% brs_plots = []
% brs_used = {station:True for station in brs.CONFIG}
% for station in brs.CONFIG:
%     if os.path.exists(event.path('brs_{}.png'.format(station))):
%         brs_plots.append(event.url('brs_{}.png'.format(station)))
%     else:
%         brs_used[station] = False
%     end
% end

% if any(status is True for status in brs_used.values()):
      <div class="row">
%     include('collapsed_plots.tpl', title ='BRS plots', id='brs', plots=brs_plots, size=5, section='main', expand=event.has_tag('BRS_GLITCH'))
      </div>
% end
% if not any(brs_used.values()):
<h3>BRS plots</h3>
% end
% for endstation, used in brs_used.items():
%     if not used:
          <br/>
          <div class="row">
          <p>{{endstation}} not using sensor correction during lockloss</p>
          </div>
%     end
% end
</div>

<!-- analog board saturation plot -->
<hr />
<div class="container">
<br />
<div class="row">
% has_tag = False
% if event.has_tag('BOARD_SAT'):
%       has_tag = True
% end
% board_plot = [event.url('board_sat.png')]
% include('collapsed_plots.tpl', title ='Analog board saturation plots', id='board_sat', plots=board_plot, size=5, expand=has_tag, section='main')
</div>
</div>

<!-- environment plot -->
<hr />
<div class="container">
<br />
<div class="row">
% has_tag = False
% has_tag = any([event.has_tag(tag) for tag in ['EARTHQUAKE', 'WINDY', 'MICROSEISMIC', 'ANTHROPOGENIC']])
% env_plots = [event.url(v['savefile']) for v in config.SEISMIC_CONFIG.values()]
% env_plots.insert(0, event.url('windy.png'))
%
% include('collapsed_plots.tpl', title ='Environment plots', id='environment', plots=env_plots, size=5, expand=has_tag, section='main')
</div>
</div>

<!-- ADS excursion plot -->
<hr />
<div class="container">
<br />
<div class="row">
% has_tag = False
% if event.has_tag('ADS_EXCURSION'):
%       has_tag = True
% end
% ads_plot = [event.url('ads.png')]
% include('collapsed_plots.tpl', title ='ADS Excursion Plot', id='ads_excursion', plots=ads_plot, size=5, expand=has_tag, section='main')
</div>
</div>

<!-- ADC overflows -->
<hr />
<div class="container">
<div class="row">
<h3>ADC Overflow Plots</h3>
<br />
</div>
<div class="row">
% include('collapsed_plots.tpl', title='ADC Overflows Overview', id='ADC Overview', plots=event_plot_urls(event, 'adc_overflow_overview'), size=6, expand=False, section='sub')
</div>
<br />
% for sys, conf in config.ADC_OVERFLOWS.items():
      <div class="row">
%     adc_name = 'adc_overflow_{}'.format(sys)
%     adc_plots = []
%     for n in range(conf['num_bits']):
%         adc_plots.extend(event_plot_urls(event, '{}_{}'.format(adc_name, n)))
%     end
%     include('collapsed_plots.tpl', title='All {} overflows'.format(sys), id=adc_name, plots=adc_plots, size=6, expand=False, section='sub')
      </div>
      <br />
% end
</div>

<!-- ASC/LSC timeseries plots -->
<hr />
<div class="container">
<div class="row">
<h3>LSC/ASC Plots</h3><br />
</div>
% for chan_group, chan_list in config.LSC_ASC_CHANNELS.items():
%     plot_list = []
%     for channel in chan_list:
%         plot_list.extend(event_plot_urls(event, channel))
%     end
      <div class="row">
%     include('collapsed_plots.tpl', title=chan_group, id=chan_group, plots=plot_list, size=3, thumbnail_diff=True, expand=False, section='sub')
      </div>
      <br />
% end
</div>

<!-- glitch heatmaps -->
<hr />
<div class="container" style="padding-bottom: 50px">
<br />
% plot_urls = []
% for ii, subsystem in enumerate(config.GLITCH_CHANNELS.keys()):
%     plot_name = 'snr_eventmap_{}'.format(subsystem)
%     plot_urls.extend(event_plot_urls(event, plot_name))
      <div class="row">
%     if not plot_urls:
         <p>No glitch plots available for {{subsystem}} subsystem</p>
%     end
      </div>
% end
<div class="row">
% include('collapsed_plots.tpl', title='Glitch Heatmaps', id='Glitch Heatmaps', plots=plot_urls, size=6, expand=False, section='main')
</div>
</div>
