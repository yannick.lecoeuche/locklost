import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data

##################################################

def plot_indicators(event, params, refined_gps=None, threshold=None):
    """Create graphs showing indicators of refined time.

    """
    t0 = event.transition_gps
    trans = event.transition_index

    channels = [
        config.GRD_STATE_N_CHANNEL,
        params['CHANNEL'],
    ]
    window = config.REFINE_PLOT_WINDOWS['WIDE']
    segment = Segment(*window).shift(t0)
    bufs = data.fetch(channels, segment)

    y_grd, t_grd = bufs[0].yt()
    y_ind, t_ind = bufs[1].yt()

    plt.rcParams['text.usetex'] = False

    isc_loss_time = event.transition_gps - t0

    for window_type, window in config.REFINE_PLOT_WINDOWS.items():
        logging.info('Making {} indicator plot'.format(window_type))

        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        # this puts ax1 on top
        ax1.set_zorder(10)
        # sets ax1 background invisible so the lower axis shows through
        ax1.patch.set_visible(False)

        # plot indicator
        color = 'blue'
        label = params['CHANNEL']
        ax1.plot(t_ind-t0, y_ind, label=label,
                 color=color, alpha=0.8, linewidth=2)
        ax1.grid(True)
        ax1.set_ylabel(params['CHANNEL'])
        ax1.yaxis.label.set_color(color)
        ax1.set_xlabel('Time [s] since {}'.format(t0))

        # add annotation/line for threshold
        if threshold is not None:
            ax1.axhline(
                threshold,
                label='threshold',
                color='green',
                linestyle='--',
                linewidth=2,
            )
            if params['THRESHOLD'] > 0:
                xyoffset = (8, 10)
                valign = 'bottom'
            else:
                xyoffset = (8, -10)
                valign = 'top'
            ax1.annotate(
                "threshold",
                xy=(ax1.get_xlim()[0], threshold),
                xycoords='data',
                xytext=xyoffset,
                textcoords='offset points',
                horizontalalignment='left',
                verticalalignment=valign,
                bbox=dict(
                    boxstyle="round", fc="w", ec="green", alpha=0.95),
                )

        # add line for refined time
        if refined_gps:
            annotation = "refined time: {}".format(refined_gps)
            atx = refined_gps - t0
            ax1.axvline(
                atx,
                label='refined time',
                color='red',
                alpha=0.8,
                linestyle='--',
                linewidth=2,
            )
        else:
            annotation = "unable to refine time"
            atx = 0
        ax1.annotate(
            annotation,
            xy=(atx, ax1.get_ylim()[1]),
            xycoords='data',
            xytext=(0, 2),
            textcoords='offset points',
            horizontalalignment='center',
            verticalalignment='bottom',
            bbox=dict(boxstyle="round", fc="w", ec="red", alpha=0.95),
            )
        ax1.set_xlim(min(atx-1, window[0]), window[1])

        # plot guardian
        color = 'orange'
        label = config.GRD_STATE_N_CHANNEL
        ax2.plot(t_grd-t0, y_grd, label=label,
                 color=color, linewidth=2)
        ax2.set_yticks(trans)
        ylim = ax2.get_ylim()
        ax2.set_ylim([ylim[0]-10, ylim[1]+10])
        ax2.set_ylabel(config.GRD_STATE_N_CHANNEL)
        ax2.yaxis.label.set_color(color)

        ax2.axhline(
            trans[0],
            label = 'state before lockloss',
            color = 'orange',
            linestyle = '--',
            linewidth = 2
        )

        ax2.axvline(
            isc_loss_time,
            label = 'lockloss time',
            color = 'orange',
            linestyle = '--',
            linewidth = 2
        )
        ax2.set_xlim(min(atx-1, window[0]), window[1])

        fig.suptitle("Lock loss refinement for {}".format(event.id))

        outfile = 'indicators_{}.png'.format(window_type)
        outpath = event.path(outfile)
        logging.debug(outpath)
        fig.savefig(outpath)


def find_transition(channel, segment, std_threshold, minimum=None):
    """Find transition in channel

    `segment` is the time segment to search, `std_threshold` is the % std
    from mean defining the transition threshold, `minimum` is an optional
    minimum value that the channel will be checked against.

    returns `threshold`, `refined_gps` tuple

    """
    refined_gps = None
    buf = data.fetch(channel, segment)[0]

    # calculate mean and std using first 5 seconds of window
    samples = int(buf.sample_rate * 5)
    lock_data = buf.data[:samples]
    lock_mean = np.mean(lock_data)
    lock_stdd = np.std(lock_data - lock_mean, ddof=1)
    logging.info("locked mean/stddev: {}/{}".format(lock_mean, lock_stdd))

    # lock loss threshold is when moves past % threshold of std
    threshold = lock_stdd * std_threshold + lock_mean
    if std_threshold > 0:
        threshold = min(threshold, max(buf.data))
    else:
        threshold = max(threshold, min(buf.data))
    logging.info("threshold: {}".format(threshold))

    # if the mean is less than the nominal full lock value then abort,
    # as this isn't a clean lock
    if minimum and lock_mean < minimum:
        logging.info("channel mean below minimum, unable to resolve time")

    else:
        if std_threshold > 0:
            inds = np.where(buf.data > threshold)[0]
        else:
            inds = np.where(buf.data < threshold)[0]

        if inds.any():
            ind = np.min(inds)
            refined_gps = buf.tarray[ind]
            logging.info("refined time: {}".format(refined_gps))
        else:
            logging.info("no threshold crossings, unable to resolve time")

    return threshold, refined_gps


def refine_time(event):
    """Refine lock loss event time.

    Find indicator channel to use, refine time with find_transition.
    Create tag and graphs and add refined time to event.

    """
    # find indicator channel to use based on guardian state
    for index, params in sorted(config.INDICATORS.items(), reverse=True):
        if event.transition_index[0] >= index:
            break

    window = config.REFINE_WINDOW
    segment = Segment(*window).shift(event.transition_gps)

    logging.info("refining time using channel: {}".format(params['CHANNEL']))
    # find threshold and refined time using indicator channel
    threshold, refined_gps = find_transition(
        [params['CHANNEL'],],
        segment,
        params['THRESHOLD'],
        params['MINIMUM'],
    )

    if not refined_gps:
        ts = 'nan'
    else:
        ts = '{:f}'.format(refined_gps)
        event.add_tag('REFINED')

    with open(event.path('refined_gps'), 'w') as f:
        f.write('{}\n'.format(ts))

    logging.info("plotting indicators...")
    plot_indicators(
        event,
        params,
        refined_gps=refined_gps,
        threshold=threshold,
    )
