import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

#################################################

CHANNEL_ENDINGS = [
    '100M_300M',
    '300M_1',
    '1_3',
]

# H1 checks single status channel that says what combination of BRSX/BRSY is
# being used
if config.IFO == 'H1':
    CONFIG = {
        'ETMX': {
            'skip_states': {65},
            'axis': 'RY',
        },
        'ETMY': {
            'skip_states': {60},
            'axis': 'RX',
        },
    }
    for station in CONFIG:
        CONFIG[station]['state_chan'] = 'H1:GRD-SEI_CONF_REQUEST_N'
        CONFIG[station]['skip_states'] |= {10, 17, 45}

# L1 checks status channels for each individual BRS (ETMs and ITMs) that says
# whether it's being used
elif config.IFO == 'L1':
    CONFIG = {
        'ITMX': {
            'state_chan': 'L1:ISI-ITMX_ST1_SENSCOR_X_FADE_CUR_CHAN_MON',
            'axis': 'RY',
        },
        'ETMX': {
            'state_chan': 'L1:ISI-ETMX_ST1_SENSCOR_Y_FADE_CUR_CHAN_MON',
            'axis': 'RY',
        },
        'ITMY': {
            'state_chan': 'L1:ISI-ITMY_ST1_SENSCOR_Y_FADE_CUR_CHAN_MON',
            'axis': 'RX',
        },
        'ETMY': {
            'state_chan': 'L1:ISI-ETMY_ST1_SENSCOR_Y_FADE_CUR_CHAN_MON',
            'axis': 'RX',
        },
    }
    for station in CONFIG:
        CONFIG[station]['skip_states'] = {8, 9}

for station in CONFIG:
    channels = []
    for ending in CHANNEL_ENDINGS:
        channels.append('{}:ISI-GND_BRS_{}_{}_BLRMS_{}'.format(config.IFO, station, CONFIG[station]['axis'], ending))
    CONFIG[station]['channels'] = channels

THRESHOLD = 15

SEARCH_WINDOW = [-30, 5]

#################################################

def check_brs(event):
    """Checks for BRS glitches at both end stations.

    Checks if more than one of three BRS channels is above threshold around
    time of lockloss (at both endstations) and creates tag/plot if it is.

    """

    plotutils.set_rcparams()

    segment = Segment(SEARCH_WINDOW).shift(int(event.gps))

    for station, params in CONFIG.items():
        # fetch all data (state and data)
        channels = [params['state_chan']] + params['channels']
        try:
            buf_dict = data.fetch(channels, segment, as_dict=True)
        except ValueError:
            logging.warning('BRS info not available for {}'.format(station))
            continue

        # check for proper state
        state_buf = buf_dict[params['state_chan']]
        t = state_buf.tarray
        state = state_buf.data[np.argmin(np.absolute(t-event.gps))]
        if state in params['skip_states']:
            logging.info('{} not using sensor correction during lockloss'.format(station))
            continue
        del buf_dict[params['state_chan']]

        # look for glitches
        max_brs = 0
        thresh_crossing = segment[1]
        for channel, buf in buf_dict.items():
            max_brs = max([max_brs, max(buf.data)])
            if any(buf.data > THRESHOLD):
                logging.info('BRS GLITCH DETECTED in {}'.format(channel))
                event.add_tag('BRS_GLITCH')
                glitch_idx = np.where(buf.data > THRESHOLD)[0][0]
                glitch_time = buf.tarray[glitch_idx]
                thresh_crossing = min(glitch_time, thresh_crossing)

        fig, ax = plt.subplots(1, figsize=(22,16))
        for channel, buf in buf_dict.items():
            t = buf.tarray
            ax.plot(
                t-event.gps,
                buf.data,
                label=channel,
                alpha=0.8,
                lw=2,
            )
        ax.axhline(
            THRESHOLD,
            linestyle='--',
            color='black',
            label='BRS glitch threshold',
            lw=5,
        )
        if thresh_crossing != segment[1]:
            plotutils.set_thresh_crossing(ax, thresh_crossing, event.gps, segment)

        ax.grid()
        ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
        ax.set_ylabel('RMS Velocity [nrad/s]')
        ax.set_ylim(0, max_brs+1)
        ax.set_xlim(t[0]-event.gps, t[-1]-event.gps)
        ax.legend(loc='best')
        ax.set_title('{} BRS BLRMS'.format(station), y=1.04)
        fig.tight_layout(pad=0.05)

        outfile_plot = 'brs_{}.png'.format(station)
        outpath_plot = event.path(outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')
