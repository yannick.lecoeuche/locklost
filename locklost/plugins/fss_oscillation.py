import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

FSS_SEARCH_WINDOW = [-5, 5]
FSS_CHANNELS = [
    '{}:PSL-FSS_FAST_MON_OUT_DQ'.format(config.IFO)
]
FSS_THRESH = 3

##############################################

def check_fss(event):
    """Checks for FSS oscillations.

    Checks FSS PD for counts below threshold and
    creates a tag/plot if below said threshold.

    """
    plotutils.set_rcparams()

    mod_window = [FSS_SEARCH_WINDOW[0], FSS_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    buf = data.fetch(FSS_CHANNELS, segment)[0]

    thresh_crossing = segment[1]
    srate = buf.sample_rate
    t = np.arange(segment[0], segment[1], 1/srate)
    thresh_ref = buf.data[np.where(t-event.gps <= -1)]
    thresh_above = np.mean(thresh_ref)+FSS_THRESH
    thresh_below = np.mean(thresh_ref)-FSS_THRESH
    glitches = np.where((thresh_ref <= thresh_below) | (thresh_ref >= thresh_above))[0]
    if any(glitches):
        glitch_time = t[glitches[0]]
        thresh_crossing = min(glitch_time, thresh_crossing)
        event.add_tag('FSS_OSCILLATION')
    else:
        logging.info('no fss oscillations')

    fig, ax = plt.subplots(1, figsize=(22,16))
    t = np.arange(segment[0], segment[1], 1/srate)
    ax.plot(
        t-event.gps,
        buf.data,
        label=buf.channel,
        alpha=0.8,
        lw=2,
    )
    ax.axhline(
        thresh_below,
        linestyle='--',
        color='black',
        label='FSS oscillation threshold',
        lw=5,
    )
    ax.axhline(
        thresh_above,
        linestyle='--',
        color='black',
        lw=5,
    )
    ax.grid()
    ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax.set_ylabel('Voltage [V]')
    ax.legend(loc='best')
    ax.set_title('FSS stability check', y=1.04)
    ax.set_xlim(segment[0]-event.gps, segment[1]-event.gps)
    if thresh_crossing != segment[1]:
        plotutils.set_thresh_crossing(ax, thresh_crossing, event.gps, segment)

    fig.tight_layout()

    fig.savefig(event.path('fss.png'))
